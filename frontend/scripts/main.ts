let todo_list: any;
let tabState: any[] = ["all", false, true];
let current_tab_index = 1;

const fetch_req = async (url: string, fetch_method: string, data: object) => {
  return await fetch(url, {
    method: fetch_method,
    mode: "cors",
    cache: "no-cache",
    credentials: "same-origin",
    headers: {
      "Content-Type": "application/json",
    },
    redirect: "follow",
    referrerPolicy: "no-referrer",
    body: JSON.stringify(data),
  });
};

window.onload = async () => {
  console.log("hello");

  document.getElementById("add-todo")?.addEventListener("click", async () => {
    let todo_text: any = document.getElementById("todo-input");
    todo_list = await fetch_req("/todos", "POST", { text: todo_text.value });
    todo_list = await todo_list.json();
    todo_list = todo_list["data"];
    console.log(todo_list);

    build_todos(todo_list, current_tab_index);
  });

  let tab_div: any = document.getElementsByClassName("nav-link");
  for (let i = 0; i < tab_div.length; ++i) {
    tab_div[i].addEventListener("click", () => {
      changeTab(i);
      current_tab_index = i;
    });
  }

  todo_list = await fetch("/todos");
  todo_list = await todo_list.json();
  todo_list = todo_list["data"];
  console.log(todo_list);

  build_todos(todo_list, current_tab_index);
};

const changeTab = (tabIndex: number) => {
  let tab_div: any = document.getElementsByClassName("nav-link");
  for (let i = 0; i < tab_div.length; ++i) {
    tab_div[i].classList.remove("active");
  }
  tab_div[tabIndex].classList.add("active");

  build_todos(todo_list, tabIndex);
};

const build_todos = (list: any, tabIndex: number) => {
  let list_div: any = document.getElementById("task-holder");
  list_div.innerHTML = "";

  let temp_list: any = list;

  temp_list = temp_list.filter((e: any) => {
    return e["is_completed"] == tabState[tabIndex];
  });

  if (tabIndex == 0) {
    temp_list = list;
  }

  let temp: string = "";
  for (let i = 0; i < temp_list.length; ++i) {
    let is_checked: string = "unchecked";
    if (temp_list[i]["is_completed"]) {
      is_checked = "checked";
    }

    temp += `
    <li class="list-group-item d-flex align-items-center border-0 mb-2 rounded" 
        style="background-color: #f4f6f7" >
        <input  class="form-check-input me-2 check-inp"
                type="checkbox"
                value="${temp_list[i]["_id"]}"
                ${is_checked} />
        <div class="${is_checked} todo-task" > ${temp_list[i]["text"]} </div>
    </li>
    `;
  }
  list_div.innerHTML = temp;

  let check_inp: any = document.getElementsByClassName("check-inp");
  for (let i = 0; i < check_inp.length; ++i) {
    check_inp[i].addEventListener("input", async (e: any) => {
      let check_state: boolean = e.target.checked;

      if (check_state) {
        await complete_task(i, e.target.defaultValue);
      } else {
        await redo_task(i, e.target.defaultValue);
      }
    });
  }
};

const complete_task = async (divIndex: number, taskID: number) => {
  document
    .getElementsByClassName("todo-task")
    [divIndex].classList.add("checked");

  await fetch_req("/todos", "DELETE", { id: taskID });

  todo_list = await fetch("/todos");
  todo_list = await todo_list.json();
  todo_list = todo_list["data"];

  build_todos(todo_list, current_tab_index);
};

const redo_task = async (divIndex: number, taskID: number) => {
  document
    .getElementsByClassName("todo-task")
    [divIndex].classList.remove("checked");

  await fetch_req("/todos", "PATCH", { id: taskID });

  todo_list = await fetch("/todos");
  todo_list = await todo_list.json();
  todo_list = todo_list["data"];

  build_todos(todo_list, current_tab_index);
};
