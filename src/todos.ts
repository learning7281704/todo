type todo_type = {
  text: string;
  id: number;
  is_completed: boolean;
};

let todos: todo_type[] = [
  {
    text: "make todo project",
    id: 1,
    is_completed: false,
  },
  {
    text: "make server",
    id: 2,
    is_completed: true,
  },
  {
    text: "add delete feature",
    id: 3,
    is_completed: false,
  },
];
export default todos;
