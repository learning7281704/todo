import express from "express";
import cors from "cors";
import dotenv from "dotenv";
dotenv.config();

import path from "path";
const __dirname = path.resolve();

import mongo_client from "./db.js";
import { ObjectId } from "mongodb";

const app = express();

// parser
app.use("/frontend", express.static(__dirname + "/frontend"));
app.use("/build", express.static(__dirname + "/build"));
app.use(express.json());

app.use(
  cors({
    origin: "*",
    methods: ["GET", "POST", "PUT", "DELETE", "PATCH"],
    allowedHeaders: [
      "Content-Type",
      "Origin",
      "X-Requested-With",
      "Accept",
      "x-client-key",
      "x-client-token",
      "x-client-secret",
      "Authorization",
    ],
    credentials: true,
  })
);

app.get("/", (req: any, res: any) => {
  res.sendFile("index.html", { root: "./" });
});

app.get("/todos", async (req: any, res: any) => {
  let todo_list: any;
  try {
    let db = mongo_client.db("todo");

    let res = db.collection("tasks").find();
    todo_list = await res.toArray();
  } catch (error) {
    console.log(error);

    res.status(500);
    res.json({ data: [] });
    return;
  }
  res.json({ data: todo_list });
});

app.post("/todos", async (req: any, res: any) => {
  let inp_data: any = req.body;
  console.log(inp_data);

  let new_todo = {
    text: inp_data["text"],
    is_completed: false,
  };

  try {
    let db = mongo_client.db("todo");

    await db.collection("tasks").insertOne(new_todo);
  } catch (error) {
    console.log(error);

    res.status(500);
    res.json({ success: false });
    return;
  }

  res.json({ success: true });
});

app.delete("/todos", async (req: any, res: any) => {
  let inp_data: any = req.body;

  let idx: number = inp_data["id"];
  try {
    let db = mongo_client.db("todo");

    await db
      .collection("tasks")
      .updateOne({ _id: new ObjectId(idx) }, { $set: { is_completed: true } });
  } catch (error) {
    console.log(error);

    res.status(500);
    res.json({ success: false });
    return;
  }

  res.json({ success: true });
});

app.patch("/todos", async (req: any, res: any) => {
  let inp_data: any = req.body;

  let idx: number = inp_data["id"];
  try {
    let db = mongo_client.db("todo");

    await db
      .collection("tasks")
      .updateOne({ _id: new ObjectId(idx) }, { $set: { is_completed: false } });
  } catch (error) {
    console.log(error);

    res.status(500);
    res.json({ success: false });
    return;
  }
  res.json({ success: true });
});

// listen for requests :)
// port infos
const port = process.env.PORT || 8000;
app.listen(port, () =>
  console.log(`App listening at http://localhost:${port}`)
);
