import { MongoClient, ObjectId, ServerApiVersion } from "mongodb";
import dotenv from "dotenv";
dotenv.config();
const uri: any = process.env.MONGODB_URL;

// Create a MongoClient with a MongoClientOptions object to set the Stable API version
const mongo_client = new MongoClient(uri, {
  serverApi: {
    version: ServerApiVersion.v1,
    strict: true,
    deprecationErrors: true,
  },
});

mongo_client
  .connect()
  .then((e) => {
    console.log("DB connected");
  })
  .catch((e) => {
    console.log(e);
    console.log("DB connection failed");
  });

export default mongo_client;
