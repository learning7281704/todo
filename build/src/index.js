var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import express from "express";
import cors from "cors";
import dotenv from "dotenv";
dotenv.config();
import path from "path";
const __dirname = path.resolve();
import mongo_client from "./db.js";
import { ObjectId } from "mongodb";
const app = express();
// parser
app.use("/frontend", express.static(__dirname + "/frontend"));
app.use("/build", express.static(__dirname + "/build"));
app.use(express.json());
app.use(cors({
    origin: "*",
    methods: ["GET", "POST", "PUT", "DELETE", "PATCH"],
    allowedHeaders: [
        "Content-Type",
        "Origin",
        "X-Requested-With",
        "Accept",
        "x-client-key",
        "x-client-token",
        "x-client-secret",
        "Authorization",
    ],
    credentials: true,
}));
app.get("/", (req, res) => {
    res.sendFile("index.html", { root: "./" });
});
app.get("/todos", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let todo_list;
    try {
        let db = mongo_client.db("todo");
        let res = db.collection("tasks").find();
        todo_list = yield res.toArray();
    }
    catch (error) {
        console.log(error);
        res.status(500);
        res.json({ data: [] });
        return;
    }
    res.json({ data: todo_list });
}));
app.post("/todos", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let inp_data = req.body;
    console.log(inp_data);
    let new_todo = {
        text: inp_data["text"],
        is_completed: false,
    };
    try {
        let db = mongo_client.db("todo");
        yield db.collection("tasks").insertOne(new_todo);
    }
    catch (error) {
        console.log(error);
        res.status(500);
        res.json({ success: false });
        return;
    }
    res.json({ success: true });
}));
app.delete("/todos", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let inp_data = req.body;
    let idx = inp_data["id"];
    try {
        let db = mongo_client.db("todo");
        yield db
            .collection("tasks")
            .updateOne({ _id: new ObjectId(idx) }, { $set: { is_completed: true } });
    }
    catch (error) {
        console.log(error);
        res.status(500);
        res.json({ success: false });
        return;
    }
    res.json({ success: true });
}));
app.patch("/todos", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let inp_data = req.body;
    let idx = inp_data["id"];
    try {
        let db = mongo_client.db("todo");
        yield db
            .collection("tasks")
            .updateOne({ _id: new ObjectId(idx) }, { $set: { is_completed: false } });
    }
    catch (error) {
        console.log(error);
        res.status(500);
        res.json({ success: false });
        return;
    }
    res.json({ success: true });
}));
// listen for requests :)
// port infos
const port = process.env.PORT || 8000;
app.listen(port, () => console.log(`App listening at http://localhost:${port}`));
