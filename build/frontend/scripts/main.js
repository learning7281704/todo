var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
let todo_list;
let tabState = ["all", false, true];
let current_tab_index = 1;
const fetch_req = (url, fetch_method, data) => __awaiter(void 0, void 0, void 0, function* () {
    return yield fetch(url, {
        method: fetch_method,
        mode: "cors",
        cache: "no-cache",
        credentials: "same-origin",
        headers: {
            "Content-Type": "application/json",
        },
        redirect: "follow",
        referrerPolicy: "no-referrer",
        body: JSON.stringify(data),
    });
});
window.onload = () => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    console.log("hello");
    (_a = document.getElementById("add-todo")) === null || _a === void 0 ? void 0 : _a.addEventListener("click", () => __awaiter(void 0, void 0, void 0, function* () {
        let todo_text = document.getElementById("todo-input");
        todo_list = yield fetch_req("/todos", "POST", { text: todo_text.value });
        todo_list = yield todo_list.json();
        todo_list = todo_list["data"];
        console.log(todo_list);
        build_todos(todo_list, current_tab_index);
    }));
    let tab_div = document.getElementsByClassName("nav-link");
    for (let i = 0; i < tab_div.length; ++i) {
        tab_div[i].addEventListener("click", () => {
            changeTab(i);
            current_tab_index = i;
        });
    }
    todo_list = yield fetch("/todos");
    todo_list = yield todo_list.json();
    todo_list = todo_list["data"];
    console.log(todo_list);
    build_todos(todo_list, current_tab_index);
});
const changeTab = (tabIndex) => {
    let tab_div = document.getElementsByClassName("nav-link");
    for (let i = 0; i < tab_div.length; ++i) {
        tab_div[i].classList.remove("active");
    }
    tab_div[tabIndex].classList.add("active");
    build_todos(todo_list, tabIndex);
};
const build_todos = (list, tabIndex) => {
    let list_div = document.getElementById("task-holder");
    list_div.innerHTML = "";
    let temp_list = list;
    temp_list = temp_list.filter((e) => {
        return e["is_completed"] == tabState[tabIndex];
    });
    if (tabIndex == 0) {
        temp_list = list;
    }
    let temp = "";
    for (let i = 0; i < temp_list.length; ++i) {
        let is_checked = "unchecked";
        if (temp_list[i]["is_completed"]) {
            is_checked = "checked";
        }
        temp += `
    <li class="list-group-item d-flex align-items-center border-0 mb-2 rounded" 
        style="background-color: #f4f6f7" >
        <input  class="form-check-input me-2 check-inp"
                type="checkbox"
                value="${temp_list[i]["_id"]}"
                ${is_checked} />
        <div class="${is_checked} todo-task" > ${temp_list[i]["text"]} </div>
    </li>
    `;
    }
    list_div.innerHTML = temp;
    let check_inp = document.getElementsByClassName("check-inp");
    for (let i = 0; i < check_inp.length; ++i) {
        check_inp[i].addEventListener("input", (e) => __awaiter(void 0, void 0, void 0, function* () {
            let check_state = e.target.checked;
            if (check_state) {
                yield complete_task(i, e.target.defaultValue);
            }
            else {
                yield redo_task(i, e.target.defaultValue);
            }
        }));
    }
};
const complete_task = (divIndex, taskID) => __awaiter(void 0, void 0, void 0, function* () {
    document
        .getElementsByClassName("todo-task")[divIndex].classList.add("checked");
    yield fetch_req("/todos", "DELETE", { id: taskID });
    todo_list = yield fetch("/todos");
    todo_list = yield todo_list.json();
    todo_list = todo_list["data"];
    build_todos(todo_list, current_tab_index);
});
const redo_task = (divIndex, taskID) => __awaiter(void 0, void 0, void 0, function* () {
    document
        .getElementsByClassName("todo-task")[divIndex].classList.remove("checked");
    yield fetch_req("/todos", "PATCH", { id: taskID });
    todo_list = yield fetch("/todos");
    todo_list = yield todo_list.json();
    todo_list = todo_list["data"];
    build_todos(todo_list, current_tab_index);
});
export {};
